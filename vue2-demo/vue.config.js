
const CopyWebpackPlugin = require('copy-webpack-plugin');
module.exports = {
    publicPath: './',
	outputDir: 'dist',
    configureWebpack: {
        plugins: [
            new CopyWebpackPlugin({
			 patterns:[
                { from: 'app.json', to: 'app.json' },
                { from: './src/preload.js', to: 'preload.js' },
                { from: 'logo.png', to: 'logo.png' },
                { from: 'README.md', to: 'README.md' },
              ]
			})
        ]
    }
}
        