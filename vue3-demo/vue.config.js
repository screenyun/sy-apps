const { defineConfig } = require('@vue/cli-service');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = defineConfig({
  publicPath: './',
  transpileDependencies: true,
  configureWebpack: {
        plugins: [
            new CopyWebpackPlugin({
			 patterns:[
                { from: 'app.json', to: 'app.json' },
                { from: './src/preload.js', to: 'preload.js' },
                { from: 'logo.png', to: 'logo.png' },
                { from: 'README.md', to: 'README.md' },
              ]
			})
        ]
    }
})
